var webpack = require('webpack');
var path = require('path');

var env = process.env.NODE_ENV;
var configEnv = env !== 'prodcution' ? 'development' : 'production';
var webpackConfig = require(`./webpack.config.${configEnv}.js`)

module.exports = webpackConfig;
