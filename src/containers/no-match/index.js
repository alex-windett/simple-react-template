import React from 'react'

class NoMatch extends React.Component {
  render() {

    const {
      children
    } = this.props;

    return (
      <div>
        <h1>404</h1>
        {children}
      </div>
    );
  }
}

export default NoMatch
