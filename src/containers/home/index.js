import React from 'react'

class Home extends React.Component {
  render() {

    const {
      children
    } = this.props;

    return (
      <div>
        <h1>Welcome to the Home Page</h1>
        {children}
      </div>
    );
  }
}

export default Home
