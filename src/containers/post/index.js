import React from 'react'
import { connect } from 'react-redux'
import store from '../../store'

class Post extends React.Component {
  render() {

    const {
      children,
      params
    } = this.props;

    return (
      <div>
        <h1>Welcome to the Post Page. Post ID = {params.postID}</h1>
        {children}
      </div>
    );
  }
}

const mapStateToProps = function(store) {
  return {
    posts: store.posts
  };
}

export default connect(mapStateToProps)(Post);
