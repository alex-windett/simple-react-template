import React from 'react'

import {
  Router,
  Route,
  browserHistory,
  IndexRoute,
  Link
} from 'react-router'

import { Provider } from 'react-redux'

import Home from './home'
import NoMatch from './no-match'
import Post from './post'
import Main from './main'
import store from '../store'

const Root = () => {
  return (
    <Provider store={store}>
      <Router history={browserHistory}>
        <Route path='/' component={Main}>
        <IndexRoute component={Home} />
        <Route path="/post/:postID" component={Post} />
        <Route path="/post" component={Post} />
        <Route path="*" component={NoMatch} />
      </Route>
      </Router>
    </Provider>
  )
}

export default Root;
