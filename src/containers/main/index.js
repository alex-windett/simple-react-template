import React from 'react'
import { Row, Col } from 'react-flexbox-grid'
import classNames from 'classnames'

import styles from './styles.css'
import Header from '../../components/header'
import Footer from '../../components/footer'

class Main extends React.Component {
  render() {
    const {
      children
    } = this.props

    const cn = classNames(
      styles.main,
      'wrapper'
    )

    return (
      <div>
        <Header />
        <Row className={cn}>
          <Col xs={12}>
            {children}
          </Col>
        </Row>
        <Footer />
      </div>
    )
  }
}

export default Main
