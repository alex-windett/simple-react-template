import {
  createStore,
  compose,
  applyMiddleware
} from 'redux'

import reducers from './reducers'

const initialState = {}

const store =  createStore(
  reducers,
  initialState,
  compose(
    applyMiddleware()
  )
);

export default store;