import {
  combineReducers 
} from 'redux';

import postReducer from './post-reducer'

// Combine Reducers
const reducers = combineReducers({
  postState: postReducer
});

export default reducers