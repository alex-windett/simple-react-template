import React from 'react'
import styles from './styles.css'
import { Link } from 'react-router'
import { Row, Col } from 'react-flexbox-grid'

const Header = () => {
  return (
    <Row className={styles.nav}>
      <Col xs={6} >
        <Link to='/'>Home</Link>
      </Col>
      <Col xs={6} >
        <Link to='/post'>Post</Link>
      </Col>
    </Row>
  );
}

export default Header
